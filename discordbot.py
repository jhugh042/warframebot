# Work with Python 3.6
import discord
import Levenshtein
import urllib.request
import json
import datetime

#functions

def getRequest(message):
	_, request = message.content.split(' ',1)
	return request

def getMissionType(mt):
	if (mt == 'MT_EXTERMINATION'):
		return 'Extermination'
	elif (mt == 'MT_SURVIVAL'):
		return 'Survival'
	elif (mt == 'MT_CAPTURE'):
		return 'Capture'
	elif (mt == 'MT_RESCUE'):
		return 'Rescue'
	elif (mt == 'MT_SABOTAGE'):
		return 'Sabotage'
	elif (mt == 'MT_EXCAVATE'):
		return 'Excavation'
	elif (mt == 'MT_MOBILE_DEFENSE'):
		return 'Mobile Defense'
	elif (mt == 'MT_TERRITORY'):
		return 'Interception'
	elif (mt == 'MT_INTEL'):
		return 'Spy'
	elif (mt == 'MT_DEFENSE'):
		return 'Defense'
	elif (mt == 'MT_RETRIEVAL'):
		return 'HiJack'
	return mt

def getFaction(fc):
	if (fc == 'FC_CORPUS'):
		return 'Corpus'
	elif (fc == 'FC_INFESTATION'):
		return 'Infestation'
	elif (fc == 'FC_GRINEER'):
		return 'Grineer'
	return fc

def getRelic(modifier):
	if (modifier == 'VoidT4'):
		return 'Axi'
	elif (modifier == 'VoidT3'):
		return 'Neo'
	elif (modifier == 'VoidT2'):
		return 'Meso'
	elif (modifier == 'VoidT1'):
		return 'Lith'
	return modifier

def getComp(comp):
	if (comp == '/Lotus/Types/Items/Research/BioComponent'):
		return 'Mutagen Mass'
	elif (comp == '/Lotus/Types/Items/Research/ChemComponent'):
		return 'Detonite Injector'
	elif (comp == '/Lotus/Types/Items/Research/EnergyComponent'):
		return 'Fieldron'
	elif (comp == '/Lotus/StoreItems/Upgrades/Mods/FusionBundles/AlertFusionBundleSmall'):
		return '80 Endo'
	elif (comp == '/Lotus/StoreItems/Upgrades/Mods/FusionBundles/AlertFusionBundleMedium'):
		return '100 Endo'
	elif (comp == '/Lotus/StoreItems/Upgrades/Mods/FusionBundles/AlertFusionBundleLarge'):
		return '150 Endo'
	elif (comp == '/Lotus/Types/Items/MiscItems/Alertium'):
		return 'Nitain Extract'
	print (comp)
	return comp.rsplit('/', 1)[-1]

def getJson(url):
	request = urllib.request.Request(url)
	response = urllib.request.urlopen(request)
	jsonData = json.loads(response.read().decode('utf-8'))
	return jsonData

def getTimeDiff(current,exp):
	expires = datetime.datetime.fromtimestamp(float(exp) / 1e3)
	difference = expires - current
	difference = divmod(difference.days * 86400 + difference.seconds, 60)
	return difference

def getPriceURL(item):
	line = 'https://api.warframe.market/v1/items/' + item.replace(' ', '_').lower() + '/statistics'
	return line


#end functions

#start discord methods

#warframe API link
URL = 'http://content.warframe.com/dynamic/worldState.php'

#Insert your discord bot token here
TOKEN = 'TOKEN'

client = discord.Client()

@client.event
async def on_message(message):
	# we do not want the bot to reply to itself
	if message.author == client.user:
        	return

	#bot will relpy hello to poster
	if message.content.startswith('!hello'):
        	msg = 'Hello {0.author.mention}'.format(message)
	        await client.send_message(message.channel, msg)
	#bot will post the commands and descriptions
	elif message.content.startswith('!help'):
		msg = '{0.author.mention} These are the commands:\n'
		msg += '!hello : Say hi!\n'
		msg += '!alerts : Gets the current alerts. (beta)\n'
		msg += '!fissures : Gets all current fissures. (beta)\n'
		msg += '!invasions : Gets the current invasion information. (beta)\n'
		msg += '!price (item): Pulls the median price from the market. (beta)\n'
		msg += '!time : Gets the time for current day and Plains of Eidolon. (incomplete)\n'
		msg += '!wiki (item): Gets the wiki page for a requested object. (beta)\n'
		newmsg = msg.format(message)
		await client.send_message(message.channel, newmsg)
		
	#gets the current or upcomming alerts
	elif message.content.startswith('!alerts'):
		jsonData = getJson(URL)
		currentTime = datetime.datetime.fromtimestamp(jsonData['Time'])
		newmsg = ''
		print(newmsg)
		for item in jsonData['SeasonInfo']['ActiveChallenges']:
			#gets the expiration of the alert
			difference = getTimeDiff(currentTime, item['Expiry']['$date']['$numberLong'])
			line = 'Expires: {}:{}\n'.format(difference[0], difference[1])

			#Gets mission type of alert
			challenge = getComp(item['Challenge'])
			line += 'Type: {}\n'.format(challenge)

			line += '==========================\n'
			newmsg += line
		await client.send_message(message.channel, newmsg) 
		
	#gets current ongoing fissures
	elif message.content.startswith('!fissures'):
		jsonData = getJson(URL)
		currentTime = datetime.datetime.fromtimestamp(jsonData['Time'])
		newmsg = ''
		for item in jsonData['ActiveMissions']:
			line = ''
			
			#gets time left on fissure
			expires = item['Expiry']['$date']['$numberLong']
			difference = getTimeDiff(currentTime,expires)

			#gets fissure mission type
			mission = getMissionType(item['MissionType'])
			relic = getRelic(item['Modifier'])

			line += '{} Fissure: {}. '.format(relic, mission)
			line += 'Expires in {}:{}.\n'.format(difference[0], difference[1])
			newmsg += line
		await client.send_message(message.channel, newmsg) 
		
	#gets currently ongoing invasions
	elif message.content.startswith('!invasions'):
		jsonData = getJson(URL)
		newmsg = ''
		for item in jsonData['Invasions']:
			line = ''
			if (item['Completed'] == False):
				#find defenders and attackers in invasions
				defenders = getFaction(item['Faction'])
				attackers = getFaction(item['AttackerMissionInfo']['faction'])
				
				#determine who is winning
				count = item['Count']
				goal = item['Goal']
				currentWinner = defenders
				if count < 0:
					currentWinner = attackers
					count = count * (-1)
				
				line += 'Attackers: {}\n'.format(attackers)
				line += 'Defenders: {}\n'.format(defenders)
				line += 'Current: {}%, {} winning.\n'.format(int((count /goal) *100), currentWinner)
				
				#get rewards for both sides
				if 'countedItems' in item['AttackerReward']:
					line += 'Attacker Rewards: '
					for rewards in item['AttackerReward']['countedItems']:
						itemType = getComp(rewards['ItemType'])
						itemCount = rewards['ItemCount']
						line += '{}({}) '.format(itemType,itemCount)
					line += '\n'
				if 'countedItems' in item['DefenderReward']:
					line += 'Defender Rewards: '
					for rewards in item['DefenderReward']['countedItems']:
						itemType = getComp(rewards['ItemType'])
						itemCount = rewards['ItemCount']
						line += '{}({}) '.format(itemType,itemCount)
				line += '\n====================\n'
			newmsg += line
		await client.send_message(message.channel,newmsg) 
		
	#gets median price of items from warframe market
	elif message.content.startswith('!price'):
		if ' ' in message.content:
			request = getRequest(message)
			jsonData = getJson(getPriceURL(request))
			newmsg = ''
			median = 0
			for item in jsonData['payload']['statistics_closed']['90days']:
				median = item['median']
			newmsg = '{} has a median price of {}.'.format(request, median)
		
		await client.send_message(message.channel, newmsg) 
	
	#TODO
	elif message.content.startswith('!time'):
		await client.send_message(message.channel, '!time has not been implemented') 
		
	#gets wiki page for item
	elif message.content.startswith('!wiki'):
		if ' ' in message.content:
			base = 'https://warframe.fandom.com/wiki/'
			filepath = 'list.txt' 
			request = getRequest(message)
			
			#uses levenshtein distance to determine the closes item
			#still needs work
			minDist = 1000
			minPhrase = 'Error'
			with open(filepath) as fp:  
				line = fp.readline()
				while line:
					currDistance = Levenshtein.distance(request, line)
					if currDistance < minDist :
						minDist = currDistance
						minPhrase = line
					line = fp.readline()
			minPhrase = minPhrase.replace(' ', '_')
			newmsg = "{}".format(base+minPhrase)
			await client.send_message(message.channel, newmsg) 



@client.event
async def on_ready():
    	print('Logged in as')
    	print(client.user.name)
    	print(client.user.id)
    	print('------')

client.run(TOKEN)
